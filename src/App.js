import './App.css';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import DefaultLayout from './components/default-layout/DefaultLayout';
import LoginPage from './pages/login/LoginPage';
import RegisterPage from './pages/register/RegisterPage';
import DashboardPage from './pages/dashboard/DashboardPage';
import PrivateRoute from './components/private-route/PrivateRoute';
import TeamsPage from './pages/teams/TeamsPage';
import PageNotFound from './pages/404/PageNotFound';
import BoardPage from './pages/board/BoardPage';
import AnalyticsPage from './pages/statistics/StatisticsPage';

function App() {
  return (
    <Router className="App">
      <Switch>
        <Redirect exact from="/" to="/dashboard" />
        <Route path="/login" component={LoginPage} />
        <Route path="/register" component={RegisterPage} />
        <DefaultLayout>
          <PrivateRoute path="/dashboard" component={DashboardPage} />
          <PrivateRoute path="/teams" component={TeamsPage} />
          <PrivateRoute path="/board/:id" component={BoardPage} />
          <PrivateRoute path="/statistics" component={AnalyticsPage} />
          {/* <Route path="/404" component={PageNotFound} />
          <Redirect to="/404" /> */}
        </DefaultLayout>
      </Switch>
    </Router>
  );
}

export default App;
