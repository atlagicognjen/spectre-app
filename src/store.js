import { configureStore } from '@reduxjs/toolkit';
import loginReducer from './components/login/loginSlice';
import registerReducer from './components/register/registerSlice';
import teamsReducer from './pages/teams/teamsSlice';
import teamMembersReducer from './components/teams-card/teamCardSlice';
import boardReducer from './pages/dashboard/dashboardSlice';
import boardSlicePage from './pages/board/boardSlice';

const store = configureStore({
  reducer: {
    login: loginReducer,
    register: registerReducer,
    teams: teamsReducer,
    teamMembers: teamMembersReducer,
    boards: boardReducer,
    board: boardSlicePage,
  },
});

export default store;
