import React, { useEffect, useState } from 'react';
import {
  Grid,
  Container,
  TextField,
  Typography,
  Button,
  Paper,
  IconButton,
  Avatar,
} from '@material-ui/core';
import { useParams } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import './board.style.css';
import { useSelector, useDispatch } from 'react-redux';
import { getBoard, editBoard, editLikes } from './boardAction';
import DeleteIcon from '@material-ui/icons/Delete';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import CommentIcon from '@material-ui/icons/Comment';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import CommentInput from '../../components/comments/CommentInput';
import AddIcon from '@material-ui/icons/Add';
import SettingsModal from '../../components/settings/SettingsModal';
import EditIcon from '@material-ui/icons/Edit';
import { commentIsEditing, commentEditingSuccess } from './boardSlice';
const useStyles = makeStyles((theme) => ({
  boardGrid: { margin: '1rem 0' },
  columnDiv: { marginBottom: '1rem' },
  column: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  iconBtn: {
    padding: '3px',
  },
  plus: {
    fontSize: '1rem',
    fontWeight: '900',
    color: 'gray',
  },
  paper: {
    minHeight: '60px',
    margin: '.5rem 0',
    padding: '10px 0',
    // backgroundColor: 'pink',
  },
  comment: {
    padding: '10px 0 5px 0',
    borderBottom: '1px solid lightgray',
  },
  avatar: {
    width: theme.spacing(3),
    height: theme.spacing(3),
    marginRight: '5px',
  },
  editDiv: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
}));
const Board = () => {
  const classes = useStyles();
  const { id } = useParams();
  const dispatch = useDispatch();

  const { isLoading, board, error, isEdditingComm } = useSelector(
    (state) => state.board
  );
  const [cardNameInput, setCardNameInput] = useState('');
  const [isEditing, setIsEditing] = useState(false);
  const [contextInput, setContextInput] = useState('');
  const [editComment, setEditComment] = useState([false, -1, -1, -1]);

  useEffect(() => {
    dispatch(getBoard(id));
    setEditComment([false, -1, -1, -1]);
  }, [dispatch]);

  const handleAddCard = (e, index) => {
    setIsEditing(true);
    const { email } = JSON.parse(sessionStorage.getItem('user'));
    const author = email.split('@')[0];
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];
    const isEmpty = [];
    listOfTemplates.forEach((template) =>
      template.cards.forEach((card) => {
        if (card.cardName === '') isEmpty.push(card.cardName);
      })
    );

    if (isEmpty.length) {
      return;
    }

    listOfTemplates[index].cards.unshift({
      cardName: '',
      likes: 0,
      author,
      comments: [],
    });

    dispatch(editBoard(listOfTemplates, id));
  };

  const handleDeleteCard = (e, columnIndex, cardIndex) => {
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];
    listOfTemplates[columnIndex].cards.splice(cardIndex, 1);
    dispatch(editBoard(listOfTemplates, id));
  };

  const handleChange = (e) => {
    const { value, name } = e.target;
    if (name === 'cardNameInput') {
      setCardNameInput(value);
    }
    if (name === 'contextInput') {
      setContextInput(value);
    }
  };
  const handleEdit = (e, columnIndex, cardIndex, commIndex) => {
    if (!isEdditingComm) {
      setEditComment([true, columnIndex, cardIndex, commIndex]);
      dispatch(commentIsEditing());
    }
    if (isEdditingComm) {
      dispatch(commentEditingSuccess());
      setEditComment([false, -1, -1, -1]);
    }
  };

  const handleSave = (e, columnIndex, cardIndex) => {
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];
    listOfTemplates[columnIndex].cards[cardIndex].cardName = cardNameInput;
    dispatch(editBoard(listOfTemplates, id));
    setIsEditing(false);
    setCardNameInput('');
  };
  const handleDelete = (e, columnIndex, cardIndex) => {
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];
    listOfTemplates[columnIndex].cards.splice(cardIndex, 1);
    dispatch(editBoard(listOfTemplates, id));
    setIsEditing(false);
    setCardNameInput('');
  };

  const handleLike = (e, columnIndex, cardIndex) => {
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];
    listOfTemplates[columnIndex].cards[cardIndex].likes += 1;
    const numOfVotes = boardCopy.numOfVotes - 1;

    if (numOfVotes < 0) return;

    dispatch(editLikes(numOfVotes, listOfTemplates, id));
  };

  const handleDeleteLike = (e, columnIndex, cardIndex) => {
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];
    listOfTemplates[columnIndex].cards[cardIndex].likes -= 1;
    const numOfVotes = boardCopy.numOfVotes + 1;

    if (numOfVotes > 6) return;

    dispatch(editLikes(numOfVotes, listOfTemplates, id));
  };

  const handleAddComment = (e, columnIndex, cardIndex) => {
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];
    let isEmptyComment = false;

    listOfTemplates.forEach((template) =>
      template.cards.forEach((card) => {
        card.comments.forEach((comment) => {
          if (comment.comment === '') {
            isEmptyComment = true;
          }
        });
      })
    );
    if (isEmptyComment) return;

    listOfTemplates[columnIndex].cards[cardIndex].comments.push({
      comment: '',
    });

    dispatch(editBoard(listOfTemplates, id));
  };

  return (
    <div className="boardDiv">
      {!board ? null : (
        <div>
          <div className={classes.appBar}>
            <Container>
              <Grid
                className={classes.boardGrid}
                container
                justify="space-between"
                alignItems="center"
              >
                <Grid xs={12} sm={6} item direction="column" container>
                  <Grid item xs={12}>
                    <Typography variant="h4">{board.boardName}</Typography>
                    <Typography variant="subtitle1">
                      ( You have{' '}
                      <span style={{ fontSize: '1.2rem' }}>
                        {board.numOfVotes}
                      </span>{' '}
                      votes left )
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    container
                    alignItems="center"
                    justify="center"
                    direction="row"
                  >
                    <Grid item xs={8}>
                      <TextField
                        autoComplete="off"
                        className="contextInput"
                        onChange={handleChange}
                        name="contextInput"
                        value={contextInput}
                        fullWidth={true}
                        placeholder="Set context of retrospective here ..."
                      />
                    </Grid>
                    <Grid item xs={4}>
                      {contextInput && (
                        <IconButton className={classes.iconBtn}>
                          <CheckCircleOutlineIcon />
                        </IconButton>
                      )}
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  className={classes.boardGrid}
                  justify="flex-end"
                  xs={12}
                  sm={6}
                  item
                  container
                >
                  <Grid item>
                    <SettingsModal />
                  </Grid>
                </Grid>
              </Grid>
            </Container>
          </div>
          <Container>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="flex-start"
            >
              {board.templates &&
                board.templates.map((column, idx) => (
                  <Grid
                    className={classes.columnDiv}
                    key={idx}
                    xs={12}
                    sm={6}
                    md={4}
                    item
                  >
                    <Container className={classes.column}>
                      <Grid item container>
                        <Grid xs={12} item>
                          <Typography variant="h5">{column.column}</Typography>
                        </Grid>
                        <Grid xs={12} item>
                          <Button
                            name={`${idx}`}
                            onClick={(e) => handleAddCard(e, idx)}
                            variant="outlined"
                            color="primary"
                            fullWidth={true}
                            disabled={isEditing ? true : false}
                          >
                            <AddIcon />
                          </Button>
                        </Grid>

                        {/* CARDS */}
                        {column.cards
                          ? column.cards.map((card, i) => (
                              <Grid xs={12} item key={i}>
                                <Paper className={classes.paper} elevation={3}>
                                  <Container>
                                    {!card.cardName ? (
                                      <Grid
                                        container
                                        direction="row"
                                        alignItems="center"
                                      >
                                        <Grid item xs={10}>
                                          <TextField
                                            autoComplete="off"
                                            variant="outlined"
                                            label="Enter card name..."
                                            name="cardNameInput"
                                            fullWidth={true}
                                            value={cardNameInput}
                                            onChange={(e) =>
                                              handleChange(e, idx, i)
                                            }
                                          />
                                        </Grid>
                                        <Grid
                                          container
                                          item
                                          justify="flex-end"
                                          alignItems="center"
                                          xs={2}
                                        >
                                          <Grid
                                            style={{
                                              justifyContent: 'center',
                                              alignItems: 'center',
                                              display: 'flex',
                                            }}
                                            item
                                            xs={12}
                                          >
                                            <IconButton
                                              className={classes.iconBtn}
                                              onClick={(e) =>
                                                handleSave(e, idx, i)
                                              }
                                            >
                                              <CheckCircleOutlineIcon
                                                style={{ color: 'green' }}
                                                fontSize="default"
                                              />
                                            </IconButton>
                                          </Grid>
                                          <Grid
                                            style={{
                                              justifyContent: 'center',
                                              alignItems: 'center',
                                              display: 'flex',
                                            }}
                                            item
                                            xs={12}
                                          >
                                            <IconButton
                                              onClick={(e) =>
                                                handleDelete(e, idx, i)
                                              }
                                              className={classes.iconBtn}
                                            >
                                              <HighlightOffIcon
                                                fontSize="default"
                                                style={{ color: 'red' }}
                                              />
                                            </IconButton>
                                          </Grid>
                                        </Grid>
                                      </Grid>
                                    ) : (
                                      <Grid
                                        container
                                        item
                                        justify="space-between"
                                        alignItems="center"
                                        direction="row"
                                      >
                                        <Grid item xs={10}>
                                          <Typography
                                            color="primary"
                                            variant="h6"
                                          >
                                            {card.cardName}
                                          </Typography>
                                        </Grid>

                                        <Grid
                                          style={{
                                            justifyContent: 'flex-end',
                                            display: 'flex',
                                          }}
                                          xs={2}
                                          item
                                        >
                                          <IconButton
                                            onClick={(e) =>
                                              handleDeleteCard(e, idx, i)
                                            }
                                            className={`${classes.iconBtn} editAndDeleteBtn`}
                                          >
                                            <DeleteIcon fontSize="small" />
                                          </IconButton>
                                        </Grid>
                                        <Grid container direction="column" item>
                                          {card.comments.map(
                                            (comment, commIndex) => (
                                              <div key={commIndex}>
                                                {comment.comment === '' ||
                                                (isEdditingComm &&
                                                  editComment[1] === idx &&
                                                  editComment[2] === i &&
                                                  editComment[3] ===
                                                    commIndex) ? (
                                                  <Grid
                                                    item
                                                    container
                                                    xs={12}
                                                    key={commIndex}
                                                  >
                                                    <CommentInput
                                                      commentValue={
                                                        comment.comment
                                                      }
                                                      columnIndex={idx}
                                                      cardIndex={i}
                                                      commentIndex={commIndex}
                                                      xs={12}
                                                    />
                                                  </Grid>
                                                ) : (
                                                  <Grid
                                                    className={classes.comment}
                                                    container
                                                    item
                                                  >
                                                    <Grid item xs={6}>
                                                      <Typography variant="subtitle2">
                                                        {comment.comment}
                                                      </Typography>
                                                    </Grid>
                                                    <Grid
                                                      className={
                                                        classes.editDiv
                                                      }
                                                      item
                                                      xs={6}
                                                    >
                                                      <IconButton
                                                        onClick={(e) =>
                                                          handleEdit(
                                                            e,
                                                            idx,
                                                            i,
                                                            commIndex
                                                          )
                                                        }
                                                        className={
                                                          classes.iconBtn
                                                        }
                                                      >
                                                        <EditIcon
                                                          fontSize="default"
                                                          style={{
                                                            opacity: '0.4',
                                                            fontSize: '1rem',
                                                          }}
                                                        />
                                                      </IconButton>
                                                    </Grid>
                                                  </Grid>
                                                )}
                                              </div>
                                            )
                                          )}
                                        </Grid>
                                        <Grid
                                          xs={12}
                                          container
                                          item
                                          align-itemms="center"
                                          direction="row"
                                          style={{ marginTop: '.5rem' }}
                                        >
                                          {' '}
                                          <Grid
                                            item
                                            container
                                            direction="row"
                                            xs={6}
                                          >
                                            {board.settings &&
                                              board.settings
                                                .showCardsAuthor && (
                                                <Grid
                                                  item
                                                  container
                                                  direction="row"
                                                  xs={6}
                                                >
                                                  <Avatar
                                                    className={classes.avatar}
                                                    alt={card.author}
                                                  ></Avatar>

                                                  <Typography variant="subtitle1">
                                                    {card.author}
                                                  </Typography>
                                                </Grid>
                                              )}
                                          </Grid>
                                          <Grid
                                            item
                                            container
                                            xs={6}
                                            justify="flex-end"
                                          >
                                            <Grid
                                              style={{
                                                justifyContent: 'flex-end',
                                                alignItems: 'center',
                                                display: 'flex',
                                              }}
                                              item
                                            >
                                              <span>
                                                {Array.from({
                                                  length: card.likes,
                                                }).map((like, i) => (
                                                  <FiberManualRecordIcon
                                                    color="primary"
                                                    key={i}
                                                    style={{
                                                      fontSize: '.5rem',
                                                    }}
                                                  />
                                                ))}
                                              </span>
                                              {card.likes ? (
                                                <span>
                                                  <IconButton
                                                    onClick={(e) =>
                                                      handleDeleteLike(
                                                        e,
                                                        idx,
                                                        i
                                                      )
                                                    }
                                                    className={classes.iconBtn}
                                                  >
                                                    <HighlightOffIcon
                                                      fontSize="default"
                                                      style={{
                                                        color: 'red',
                                                        fontSize: '1rem',
                                                      }}
                                                    />
                                                  </IconButton>
                                                </span>
                                              ) : null}
                                              <IconButton
                                                disabled={
                                                  board.settings.disableVoting
                                                    ? true
                                                    : false
                                                }
                                                onClick={(e) => {
                                                  handleLike(e, idx, i);
                                                }}
                                                className={classes.iconBtn}
                                              >
                                                <ThumbUpIcon fontSize="small" />
                                              </IconButton>
                                              {board.settings &&
                                              !board.settings.hideVoteCount ? (
                                                <Typography variant="subtitle2">
                                                  {card.likes}
                                                </Typography>
                                              ) : null}
                                            </Grid>

                                            <Grid
                                              style={{
                                                justifyContent: 'flex-end',
                                                alignItems: 'center',
                                                display: 'flex',
                                              }}
                                              item
                                            >
                                              <IconButton
                                                className={classes.iconBtn}
                                                onClick={(e) =>
                                                  handleAddComment(e, idx, i)
                                                }
                                              >
                                                <CommentIcon fontSize="small" />
                                              </IconButton>{' '}
                                              <Typography variant="subtitle2">
                                                {card.comments.length}
                                              </Typography>
                                            </Grid>
                                          </Grid>
                                        </Grid>
                                      </Grid>
                                    )}
                                  </Container>
                                </Paper>
                              </Grid>
                            ))
                          : null}
                      </Grid>
                    </Container>
                  </Grid>
                ))}
            </Grid>
          </Container>
        </div>
      )}
    </div>
  );
};

export default Board;
