import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoading: false,
  board: {},
  error: '',
  isEdditingComm: false,
};
const boardPageSlice = createSlice({
  name: 'board',
  initialState,
  reducers: {
    boardPending: (state) => {
      state.isLoading = true;
    },
    boardSuccess: (state, action) => {
      state.isLoading = false;
      state.board = action.payload;
    },
    boardError: (state, action) => {
      state.isLoading = false;
      state.board = {};
      state.error = action.payload;
    },
    editBoardPending: (state) => {
      state.isLoading = true;
    },
    editBoardSuccess: (state, action) => {
      state.isLoading = false;
      state.board = action.payload;
    },
    editBoardError: (state, action) => {
      state.isLoading = false;
      state.board = {};
      state.error = action.payload;
    },
    commentIsEditing: (state, action) => {
      state.isEdditingComm = true;
    },
    commentEditingSuccess: (state, action) => {
      state.isEdditingComm = false;
    },
  },
});

const { reducer, actions } = boardPageSlice;
export const {
  boardPending,
  boardSuccess,
  boardError,
  editBoardPending,
  editBoardSuccess,
  editBoardError,
  commentIsEditing,
  commentEditingSuccess,
} = actions;
export default reducer;
