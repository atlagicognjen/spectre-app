import {
  boardPending,
  boardSuccess,
  boardError,
  editBoardPending,
  editBoardSuccess,
  editBoardError,
} from './boardSlice';
import axios from 'axios';

export const getBoard = (id) => async (dispatch) => {
  dispatch(boardPending());

  try {
    const { data } = await axios.get(`http://localhost:5000/boards/${id}`);

    dispatch(boardSuccess(data));
  } catch (error) {
    dispatch(boardError(error.message));
  }
};

export const editBoard = (clientData, id) => async (dispatch) => {
  dispatch(editBoardPending());

  try {
    const { data } = await axios.patch(`http://localhost:5000/boards/${id}`, {
      templates: clientData,
    });

    dispatch(editBoardSuccess(data));
  } catch (error) {
    dispatch(editBoardError(error.message));
  }
};

export const editLikes = (numOfVotes, templates, id) => async (dispatch) => {
  dispatch(editBoardPending());

  try {
    const { data } = await axios.patch(`http://localhost:5000/boards/${id}`, {
      numOfVotes,
      templates,
    });

    dispatch(editBoardSuccess(data));
  } catch (error) {
    dispatch(editBoardError(error.message));
  }
};

export const editSettings = (settings, id) => async (dispatch) => {
  dispatch(editBoardPending());

  try {
    const { data } = await axios.patch(`http://localhost:5000/boards/${id}`, {
      settings,
    });

    dispatch(editBoardSuccess(data));
  } catch (error) {
    dispatch(editBoardError(error.message));
  }
};
