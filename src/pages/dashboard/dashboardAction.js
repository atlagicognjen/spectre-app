import {
  newBoardPending,
  newBoardSuccess,
  newBoardError,
  getBoardsPending,
  getBoardsSuccess,
  getBoardsError,
} from './dashboardSlice';
import axios from 'axios';

export const createNewBoard = (settingsData) => async (dispatch) => {
  dispatch(newBoardPending());

  try {
    const { data } = await axios.post('http://localhost:5000/boards', {
      ...settingsData,
    });

    dispatch(newBoardSuccess(data));
  } catch (error) {
    dispatch(newBoardError());
  }
};

export const getBoards = () => async (dispatch) => {
  dispatch(getBoardsPending());

  try {
    const { data } = await axios.get('http://localhost:5000/boards');

    dispatch(getBoardsSuccess(data));
  } catch (error) {
    dispatch(getBoardsError());
  }
};

export const deleteBoard = (id) => async (dispatch) => {
  try {
    const { data } = await axios.delete(`http://localhost:5000/boards/${id}`);
  } catch (error) {
    dispatch(getBoardsError(error));
  }
};
