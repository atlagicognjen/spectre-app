import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoading: false,
  boards: [],
  board: {},
  error: '',
};

const boardSlice = createSlice({
  name: 'boards',
  initialState,
  reducers: {
    newBoardPending: (state) => {
      state.isLoading = true;
    },
    newBoardSuccess: (state, action) => {
      state.isLoading = false;
      state.board = action.payload;
    },
    newBoardError: (state, action) => {
      state.isLoading = false;
      state.board = {};
      state.error = action.payload;
    },
    getBoardsPending: (state) => {
      state.isLoading = true;
    },
    getBoardsSuccess: (state, action) => {
      state.isLoading = false;
      state.boards = action.payload;
    },
    getBoardsError: (state, action) => {
      state.isLoading = false;
      state.boards = [];
      state.error = action.payload;
    },
  },
});

const { reducer, actions } = boardSlice;

export const {
  newBoardPending,
  newBoardSuccess,
  newBoardError,
  getBoardsPending,
  getBoardsSuccess,
  getBoardsError,
} = actions;

export default reducer;
