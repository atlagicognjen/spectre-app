import React, { useState, useEffect } from 'react';
import {
  Typography,
  Container,
  TextField,
  Grid,
  FormGroup,
  FormControlLabel,
  Paper,
  Button,
  Checkbox,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ModalButton from '../../components/modal/Modal';
import { useSelector, useDispatch } from 'react-redux';
import { createNewBoard, getBoards } from './dashboardAction';
import BoardCard from '../../components/boardCard/BoardCard';

const useStyles = makeStyles((theme) => ({
  heading: {
    marginTop: '3rem',
  },
  root: { width: '100%' },
  formItems: {
    padding: '.5rem',
  },
  addTemplate: { display: 'flex', justifyContent: 'flex-end' },
  teams: {
    marginTop: '3rem',
  },
  addTeam: {
    maxHeight: '200px',
    width: '90%',
    padding: '20px 0',
    border: '2px dashed gray',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    color: 'gray',
    '&:hover': {
      border: '2px dashed #3f51b5 ',
    },
    marginBottom: '2rem',
  },
  formItems: {
    margin: theme.spacing(2),
  },
  templates: {
    margin: theme.spacing(2),
  },
  settings: {
    margin: theme.spacing(2),
  },

  cardDiv: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
}));

const DashboardPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const styles = {
    borderRadius: '100%',
    fontSize: '2rem',
    marginBottom: '5px',
    color: 'gray',
    '&:hover': {
      backgroundColor: '#3f51b5',
    },
  };

  const { isLoading, boards, board, error } = useSelector(
    (state) => state.boards
  );

  const [boardName, SetBoardName] = useState('');
  const [numOfVotes, setNumOfVotes] = useState(6);
  const [templates, setTemplates] = useState([{ column: '', cards: [] }]);
  const [settings, setSettings] = useState({
    hideCards: false,
    disableVoting: false,
    hideVoteCount: false,
    showCardsAuthor: true,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === 'boardName') {
      SetBoardName(value);
    } else if (name === 'numOfVotes') {
      setNumOfVotes(value);
    }
  };

  const handleChangeSettings = (e) => {
    const { name, checked } = e.target;

    setSettings({ ...settings, [name]: checked });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const today = new Date();
    const createdAt = today.toLocaleDateString();
    const boardCreator = JSON.parse(sessionStorage.getItem('user'));
    const data = {
      boardName,
      numOfVotes,
      templates,
      settings,
      createdAt,
      boardCreator,
    };
    dispatch(createNewBoard(data));
    SetBoardName('');
    setNumOfVotes(6);
    setTemplates([{ column: '', cards: [] }]);
    setSettings({
      hideCards: false,
      disableVoting: false,
      hideVoteCount: false,
      showCardsAuthor: true,
    });
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...templates];

    list[index][name] = value;
    setTemplates(list);
  };

  const handleAddColumn = () => {
    const list = [...templates];
    setTemplates([
      ...templates,
      {
        column: '',
        cards: [],
      },
    ]);
  };

  useEffect(() => {
    dispatch(getBoards());
  }, [dispatch, board]);

  return (
    <Container>
      <div className={classes.heading}>
        <Typography variant="h4">My Boards</Typography>
      </div>

      <Grid
        container
        className={classes.teams}
        justify="space-evenly"
        alignItems="flex-start"
      >
        <Grid item xs={12} sm={6} md={4} lg={3} className={classes.addTeam}>
          <ModalButton
            className={classes.btn}
            heading="Team name"
            variant="contained"
            btnText="+"
            submit={handleSubmit}
            styles={styles}
          >
            <Grid
              justify="center"
              alignItems="center"
              direction="column"
              item
              container
            >
              <Grid item className={classes.formItems}>
                <Typography color="primary" variant="h4">
                  Board info
                </Typography>
              </Grid>
              <Grid item className={classes.formItems}>
                <TextField
                  fullWidth={true}
                  id="boardName"
                  label="Board name"
                  type="text"
                  value={boardName}
                  name="boardName"
                  onChange={handleChange}
                  required={true}
                  variant="outlined"
                />
              </Grid>
              <Grid item className={classes.formItems}>
                <TextField
                  fullWidth={true}
                  id="numOfVotes"
                  label="Max number of votes"
                  type="number"
                  value={numOfVotes}
                  name="numOfVotes"
                  onChange={handleChange}
                  required={true}
                  variant="outlined"
                />
              </Grid>

              <Grid
                style={{ textAlign: 'center' }}
                item
                className={classes.settings}
              >
                <Typography color="primary" variant="h6">
                  Templates and Settings
                </Typography>
                <Grid
                  container
                  justiyfy="center"
                  alignItems="center"
                  direction="column"
                >
                  <Grid item>
                    {templates.map((input, i) => (
                      <Grid item key={i} className={classes.formItems}>
                        <TextField
                          id={`${i}`}
                          label={`Column ${i + 1}`}
                          type="text"
                          value={input.column}
                          name="column"
                          onChange={(e) => handleInputChange(e, i)}
                          required={true}
                          variant="outlined"
                        />
                      </Grid>
                    ))}

                    <Grid item className={classes.addTemplate}>
                      <Button
                        color="primary"
                        onClick={handleAddColumn}
                        className={classes.formItems}
                      >
                        Add column
                      </Button>
                    </Grid>
                  </Grid>
                  <Grid item>
                    <FormGroup className={classes.root} row={false}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={settings.hideCards}
                            onChange={handleChangeSettings}
                            name="hideCards"
                            color="primary"
                          />
                        }
                        label="Hide cards initialy"
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={settings.disableVoting}
                            onChange={handleChangeSettings}
                            name="disableVoting"
                            color="primary"
                          />
                        }
                        label="Disable voting initialy"
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={settings.hideVoteCount}
                            onChange={handleChangeSettings}
                            name="hideVoteCount"
                            color="primary"
                          />
                        }
                        label="Hide vote count"
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={settings.showCardsAuthor}
                            onChange={handleChangeSettings}
                            name="showCardsAuthor"
                            color="primary"
                          />
                        }
                        label="Show card's author"
                      />
                    </FormGroup>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </ModalButton>
          <Typography variant="subtitle2">Add board</Typography>
        </Grid>
        {boards.length
          ? boards.map((board, i) => (
              <Grid
                key={i}
                item
                xs={12}
                sm={6}
                md={4}
                lg={3}
                item
                className={classes.cardDiv}
              >
                <BoardCard board={board} />
              </Grid>
            ))
          : null}
      </Grid>
    </Container>
  );
};

export default DashboardPage;
