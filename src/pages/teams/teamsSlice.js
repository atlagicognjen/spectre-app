import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoading: false,
  newTeam: {},
  teams: [],
  error: '',
  isDeleted: false,
  isEditing: null,
};

const teamsSlice = createSlice({
  name: 'teams',
  initialState,
  reducers: {
    newTeamPending: (state) => {
      state.isLoading = true;
    },
    newTeamSuccess: (state, action) => {
      state.isLoading = false;
      state.newTeam = action.payload;
    },
    newTeamError: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
      state.newTeam = {};
    },
    getTeamsPending: (state) => {
      state.isLoading = true;
    },
    getTeamsSuccess: (state, action) => {
      state.isLoading = false;
      state.teams = action.payload;
    },
    getTeamsError: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
      state.teams = [];
      state.isDeleted = false;
    },

    deleteTeams: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
      state.isDeleted = true;
    },
    isEditingTeamId: (state, action) => {
      state.isEditing = action.payload;
    },
    editTeamPending: (state, action) => {
      state.isLoading = true;
      state.isEditing = action.payload;
      state.error = '';
    },
    editTeamSuccess: (state, action) => {
      state.isLoading = false;
      state.isEditing = null;
      state.error = '';
    },
    editTeamError: (state, action) => {
      state.isLoading = false;
      state.isEditing = null;
      state.error = action.payload;
    },
    teamsReset: (state, action) => {
      state.isLoading = false;
      state.error = '';
      state.isDeleted = false;
    },
  },
});

const { reducer, actions } = teamsSlice;

export const {
  newTeamPending,
  newTeamSuccess,
  newTeamError,
  getTeamsPending,
  getTeamsSuccess,
  getTeamsError,
  teamsReset,
  deleteTeams,
  isEditingTeamId,
  editTeamPending,
  editTeamSuccess,
  editTeamError,
} = actions;

export default reducer;
