import React, { useState, useEffect } from 'react';
import { Typography, Container, TextField, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ModalButton from '../../components/modal/Modal';

import { useSelector, useDispatch } from 'react-redux';
import { createNewTeam, getAllTeams } from './teamsActions';
import TeamsCard from '../../components/teams-card/TeamsCard';
import { teamsReset } from './teamsSlice';
const useStyles = makeStyles((theme) => ({
  heading: {
    marginTop: '3rem',
  },
  teams: {
    marginTop: '3rem',
  },
  addTeam: {
    maxHeight: '200px',
    width: '90%',
    padding: '20px 0',
    border: '2px dashed gray',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    color: 'gray',
    '&:hover': {
      border: '2px dashed #3f51b5 ',
    },
    marginBottom: '2rem',
  },
  formItems: {
    margin: theme.spacing(2),
  },
  cardDiv: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
}));

const TeamsPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const styles = {
    borderRadius: '100%',
    fontSize: '2rem',
    marginBottom: '5px',
    color: 'gray',
    '&:hover': {
      backgroundColor: '#3f51b5',
    },
    paper: {
      marginTop: '2rem',
      padding: '.5rem 2rem 2rem 2rem',
    },
  };
  const [teamName, SetTeamName] = useState('');

  const { isLoading, newTeam, error, teams, isDeleted, isEditing } =
    useSelector((state) => state.teams);

  useEffect(() => {
    dispatch(teamsReset());
    dispatch(getAllTeams());
  }, [newTeam, isDeleted, isEditing, dispatch]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === 'teamName') {
      SetTeamName(value);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(createNewTeam(teamName));
    SetTeamName('');
  };

  return (
    <Container>
      <div className={classes.heading}>
        <Typography variant="h4">My Teams</Typography>
      </div>

      <Grid
        container
        className={classes.teams}
        justify="space-evenly"
        alignItems="flex-start"
      >
        <Grid item xs={12} sm={6} md={4} className={classes.addTeam}>
          <ModalButton
            className={classes.btn}
            heading="Team name"
            variant="contained"
            btnText="+"
            submit={handleSubmit}
            styles={styles}
          >
            <Grid
              justify="center"
              alignItems="center"
              direction="column"
              item
              container
            >
              <Grid item className={classes.formItems}>
                <Typography color="primary" variant="h4">
                  Team name
                </Typography>
              </Grid>
              <Grid item className={classes.formItems}>
                <TextField
                  fullWidth={true}
                  id="standard-password-input"
                  label="Team name"
                  type="text"
                  value={teamName}
                  name="teamName"
                  onChange={handleChange}
                  required={true}
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </ModalButton>
          <Typography variant="subtitle2">Add team</Typography>
        </Grid>

        {teams.map((team) => (
          <Grid
            className={classes.cardDiv}
            xs={12}
            sm={6}
            md={4}
            item
            key={team.id}
          >
            <TeamsCard id={team.id} team={team} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default TeamsPage;
