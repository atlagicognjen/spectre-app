import {
  newTeamPending,
  newTeamSuccess,
  newTeamError,
  getTeamsPending,
  getTeamsSuccess,
  getTeamsError,
  deleteTeams,
  editTeamPending,
  editTeamSuccess,
  editTeamError,
} from './teamsSlice';
import axios from 'axios';

export const createNewTeam = (teamName) => async (dispatch) => {
  dispatch(newTeamPending());

  try {
    const { data } = await axios.post('http://localhost:5000/teams', {
      teamName,
      teamMembers: [],
    });
    console.log(data);
    if (!data) return dispatch(newTeamError("Can't create team"));

    dispatch(newTeamSuccess({ teamName }));
  } catch (error) {
    dispatch(newTeamError(error.message));
  }
};
export const getAllTeams = () => async (dispatch) => {
  dispatch(getTeamsPending());

  try {
    const { data } = await axios.get('http://localhost:5000/teams');
    dispatch(getTeamsSuccess(data));
  } catch (error) {
    dispatch(getTeamsError(error.message));
  }
};

export const deleteTeam = (id) => async (dispatch) => {
  const { data } = await axios.delete(`http://localhost:5000/teams/${id}`);
  dispatch(deleteTeams(true));
  console.log(data);
};

export const editTeam = ({ id, teamName, inputs }) => async (dispatch) => {
  dispatch(editTeamPending());
  console.log(teamName, inputs);
  try {
    if (teamName && inputs.length) {
      const { data } = await axios.patch(`http://localhost:5000/teams/${id}`, {
        teamMembers: inputs,
        teamName,
      });
    }
    if (!teamName) {
      const { data } = await axios.patch(`http://localhost:5000/teams/${id}`, {
        teamMembers: inputs,
      });
    }
    if (!inputs.length) {
      const { data } = await axios.patch(`http://localhost:5000/teams/${id}`, {
        teamName,
      });
    }

    dispatch(editTeamSuccess());
  } catch (error) {
    dispatch(editTeamError(error.message));
  }
};
