import React, { useState, useEffect } from 'react';
import { Container, Grid, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { getBoards } from '../dashboard/dashboardAction';
import { getAllTeams } from '../teams/teamsActions';
import TotalChart from '../../components/charts/TotalChart';
import BoardsChart from '../../components/charts/BoardsChart';
import TeamsChart from '../../components/charts/TeamsChart';
const useStyles = makeStyles((theme) => ({
  root: {},
  heading: {
    marginTop: '3rem',
  },
  menuDiv: {
    marginTop: '3rem',
    paddingRight: '3rem',
  },
  statisticsDiv: {
    marginTop: '3rem',
  },
  buttonDiv: {
    margin: '1rem 0',
  },
  button: {
    padding: '2rem',
  },
}));

const StatisticsPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { boards, board } = useSelector((state) => state.boards);
  const { teams } = useSelector((state) => state.teams);
  const [chartInfo, setChartInfo] = useState({
    numOfBoards: 0,
    numOfTeams: 0,
    boardNames: [],
  });

  const [chartActive, setChartActive] = useState('total');

  useEffect(() => {
    dispatch(getBoards());
    dispatch(getAllTeams());
    const boardsCopy = JSON.parse(JSON.stringify(boards));
    const boardNames = boardsCopy.map((board) => board.boardName);

    //state

    setChartInfo({
      numOfBoards: boards.length,
      numOfTeams: teams.length,
      boardNames,
    });
  }, [dispatch]);

  const handleClick = (e) => {
    const { name } = e.currentTarget;

    if (name === 'total') {
      setChartActive(name);
    } else if (name === 'boards') {
      setChartActive(name);
    } else if (name === 'teams') {
      setChartActive(name);
    }
  };

  return (
    <Container>
      <Grid
        className={classes.root}
        container
        alignItems="center"
        direction="row"
      >
        <Grid className={classes.heading} item xs={12}>
          <Typography variant="h4">Statistics</Typography>
        </Grid>
        <Grid className={classes.menuDiv} item container xs={4}>
          <Grid className={classes.buttonDiv} item xs={12}>
            <Button
              className={classes.button}
              onClick={handleClick}
              fullWidth={true}
              variant={chartActive === 'total' ? 'contained' : 'outlined'}
              color="primary"
              name="total"
            >
              Total Boards and Teams
            </Button>
          </Grid>
          <Grid className={classes.buttonDiv} item xs={12}>
            <Button
              className={classes.button}
              onClick={handleClick}
              fullWidth={true}
              variant={chartActive === 'boards' ? 'contained' : 'outlined'}
              color="primary"
              name="boards"
            >
              Boards
            </Button>
          </Grid>
          <Grid className={classes.buttonDiv} item xs={12}>
            <Button
              className={classes.button}
              onClick={handleClick}
              fullWidth={true}
              variant={chartActive === 'teams' ? 'contained' : 'outlined'}
              color="primary"
              name="teams"
            >
              Teams
            </Button>
          </Grid>
        </Grid>
        <Grid className={classes.statisticsDiv} item xs={8}>
          {chartActive === 'total' && (
            <TotalChart
              numOfBoards={chartInfo.numOfBoards}
              numOfTeams={chartInfo.numOfTeams}
            />
          )}
          {chartActive === 'boards' && (
            <BoardsChart
              boardNames={chartInfo.boardNames}
              numOfTeams={chartInfo.numOfTeams}
            />
          )}
          {chartActive === 'teams' && (
            <TeamsChart boardNames={chartInfo.boardNames} />
          )}
        </Grid>
      </Grid>
    </Container>
  );
};

export default StatisticsPage;
