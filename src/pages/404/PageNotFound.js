import { Grid } from '@material-ui/core';
import React from 'react';

const PageNotFound = () => {
  return (
    <Grid justify="center" alignItems="center" container>
      <Grid item>
        <img style={{ marginTop: '2%' }} src="./imgs/not-found.svg" alt="" />
      </Grid>
    </Grid>
  );
};

export default PageNotFound;
