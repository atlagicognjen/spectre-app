import React from 'react';
import { Container, Paper, Grid, Typography } from '@material-ui/core';
import Login from '../../components/login/Login';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundImage: 'url(/imgs/login.svg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
  },
  loginDiv: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  paper: {
    padding: '50px 20px',
  },
  logo: {
    margin: theme.spacing(3),
  },
}));
const LoginPage = () => {
  const classes = useStyles();
  return (
    <Container className={classes.root}>
      <Typography className={classes.logo} variant="h2">
        Spectre
      </Typography>
      <Paper elevation={3} className={classes.paper}>
        <Grid container className={classes.loginDiv}>
          <Grid item xs={12}>
            <Typography variant="h4">Login</Typography>
          </Grid>
          <Grid item xs={12}>
            <Login />
          </Grid>
          <Grid item xs={12}>
            <Typography variant="subtitle2">
              Don't have account?{' '}
              <Link color="primary" to="/register">
                Register
              </Link>
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    </Container>
  );
};

export default LoginPage;
