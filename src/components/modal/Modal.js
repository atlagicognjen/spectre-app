import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Button, Paper, Grid } from '@material-ui/core';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  paper: {
    overflowY: 'auto',
    maxHeight: '90vh',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function ModalButton({
  submit,
  children,
  btnText,
  styles,
  fullWidth,
}) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const { newTeam } = useSelector((state) => state.teams);
  const { board } = useSelector((state) => state.boards);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (newTeam) {
      handleClose();
    }
    if (board) {
      handleClose();
    }
  }, [newTeam, board]);
  return (
    <div>
      <Button
        fullWidth={fullWidth}
        className={classes.button}
        variant="contained"
        type="button"
        onClick={handleOpen}
        style={styles}
      >
        {btnText}
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Paper elevation={3} className={classes.paper}>
            <form onSubmit={submit}>
              <Grid container>
                <Grid item>{children}</Grid>
              </Grid>
              <Grid item container justify="space-between">
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    name="save"
                    type="submit"
                    size="large"
                  >
                    Save
                  </Button>
                </Grid>
                <Grid item>
                  <Button onClick={handleClose} type="button" size="large">
                    Cancel
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Paper>
        </Fade>
      </Modal>
    </div>
  );
}
