import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const isAuth = sessionStorage.getItem('user');

  return (
    <Route
      {...rest}
      render={() => (isAuth ? <Component /> : <Redirect to="/login" />)}
    />
  );
};

export default PrivateRoute;
