import React, { useEffect, useState } from 'react';
import { Pie } from 'react-chartjs-2';
import { useSelector } from 'react-redux';

const TotalChart = () => {
  const { teams } = useSelector((state) => state.teams);
  const mdColors = [
    '#C0392B',
    '#9B59B6',
    '#2980B9',
    '#7986CB',
    '#2980B9',
    '#2980B9',
    '#1E8449',
    '#AAB7B8',
    '#AAB7B8',
    '#C0392B',
    '#9B59B6',
    '#2980B9',
    '#7986CB',
    '#2980B9',
    '#2980B9',
    '#1E8449',
    '#AAB7B8',
    '#AAB7B8',
  ];

  const colors = (colorsArray) => {
    const colorsArr = [];
    for (let i = 0; i < teams.length; i++) {
      colorsArr.push(colorsArray[i]);
    }
    return colorsArr;
  };
  return (
    <div>
      <Pie
        data={{
          labels: [...teams.map((team) => `${team.teamName} members `)],
          datasets: [
            {
              label: 'Team members',
              data: [...teams.map((team) => team.teamMembers.length)],

              backgroundColor: colors(mdColors),
            },
          ],
        }}
        height={350}
        width="100%"
        options={{
          maintainAspectRatio: false,
          plugins: {
            legend: { labels: { font: { size: 16 } } },
          },
        }}
      />
    </div>
  );
};

export default TotalChart;
