import React, { useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import { useSelector } from 'react-redux';

const BoardsChart = ({ boardNames }) => {
  const { boards } = useSelector((state) => state.boards);
  const boardCopy = JSON.parse(JSON.stringify(boards));
  const cards = [
    ...boardCopy.map((board) =>
      board.templates.map((template) => template.cards.length)
    ),
  ]
    .map((card) => card.reduce((a, b) => Number(a) + Number(b), 0))
    .map((card) => (card ? card : 0));

  const likes = [
    ...boardCopy.map((board) =>
      board.templates.map((template) =>
        template.cards.map((card) => card.likes)
      )
    ),
  ]
    .map((likeArr) =>
      likeArr.map((like) =>
        like.reduce((a, b) => {
          return Number(a) + Number(b);
        }, 0)
      )
    )
    .map((like) => like.reduce((a, b) => a + b));

  const comments = [
    ...boardCopy.map((board) =>
      board.templates.map((template) =>
        template.cards.map((card) => card.comments.length)
      )
    ),
  ]
    .map((comments) =>
      comments.map((comment) =>
        comment.reduce((a, b) => Number(a) + Number(b), 0)
      )
    )
    .map((comment) => comment.reduce((a, b) => a + b));

  useEffect(() => {
    console.log(comments);
  }, []);

  return (
    <div>
      <Bar
        data={{
          labels: [...boardNames],
          datasets: [
            {
              label: 'cards',
              data: [...cards],

              backgroundColor: '#006400',
            },
            {
              label: 'likes',

              data: [...likes],
              backgroundColor: '#8A2BE2',
            },
            {
              label: 'comments',
              data: [...comments],

              backgroundColor: '#FF8C00',
            },
          ],
        }}
        height={350}
        width="100%"
        options={{
          maintainAspectRatio: false,
          plugins: {
            legend: { labels: { font: { size: 16 } } },
          },
        }}
      />
    </div>
  );
};

export default BoardsChart;
