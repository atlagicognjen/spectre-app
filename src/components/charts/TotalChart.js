import React, { useState } from 'react';
import { Doughnut } from 'react-chartjs-2';

const TotalChart = ({ numOfBoards, numOfTeams }) => {
  return (
    <div>
      <Doughnut
        data={{
          labels: [`Boards ${numOfBoards}`, `Teams ${numOfTeams}`],
          datasets: [
            {
              label: 'Boards',
              data: [numOfBoards, numOfTeams],

              backgroundColor: ['#006400', '#8A2BE2'],
            },
          ],
        }}
        height={350}
        width="100%"
        options={{
          maintainAspectRatio: false,
          plugins: {
            legend: { labels: { font: { size: 16 } } },
          },
        }}
      />
    </div>
  );
};

export default TotalChart;
