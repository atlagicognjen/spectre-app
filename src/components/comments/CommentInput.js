import React, { useEffect, useState } from 'react';
import { Grid, Container, TextField, IconButton } from '@material-ui/core';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { editBoard } from '../../pages/board/boardAction';
import { useParams } from 'react-router';
import {
  commentIsEditing,
  commentEditingSuccess,
} from '../../pages/board/boardSlice';
const useStyles = makeStyles((theme) => ({
  iconBtn: {
    padding: '3px',
  },
  root: { width: '100%', margin: '.5rem' },
}));
const CommentInput = ({
  columnIndex,
  cardIndex,
  commentIndex,
  commentValue,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [comment, setComment] = useState(commentValue);
  const { id } = useParams();

  const { isLoading, board, error, isEdditingComm } = useSelector(
    (state) => state.board
  );

  const handleSave = (e) => {
    if (!comment) return;
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];

    const comentsLength =
      listOfTemplates[columnIndex].cards[cardIndex].comments.length - 1;
    listOfTemplates[columnIndex].cards[cardIndex].comments[
      comentsLength
    ].comment = comment;

    dispatch(editBoard(listOfTemplates, id));
    setComment('');
    dispatch(commentEditingSuccess());
  };

  const handleEdit = (e) => {
    if (!comment) return;
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];

    const comentsLength =
      listOfTemplates[columnIndex].cards[cardIndex].comments.length - 1;
    listOfTemplates[columnIndex].cards[cardIndex].comments[
      commentIndex
    ].comment = comment;

    dispatch(editBoard(listOfTemplates, id));
    setComment('');
    dispatch(commentEditingSuccess());
  };

  const handleChange = (e) => {
    setComment(e.target.value);
  };

  const handleDelete = (e, columnIndex, cardIndex, commentIndex) => {
    const boardCopy = JSON.parse(JSON.stringify(board));
    const listOfTemplates = [...boardCopy.templates];
    listOfTemplates[columnIndex].cards[cardIndex].comments.splice(
      commentIndex,
      1
    );
    dispatch(editBoard(listOfTemplates, id));
    dispatch(commentEditingSuccess());
  };

  return (
    <Grid
      className={classes.root}
      container
      direction="row"
      alignItems="center"
    >
      <Grid item xs={10}>
        <TextField
          variant="outlined"
          label="Comment..."
          fullWidth={true}
          value={comment}
          onChange={(e) => handleChange(e)}
        />
      </Grid>
      <Grid container item justify="flex-end" alignItems="center" xs={2}>
        <Grid
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
          }}
          item
          xs={12}
        >
          <IconButton
            className={classes.iconBtn}
            onClick={isEdditingComm ? handleEdit : handleSave}
          >
            <CheckCircleOutlineIcon
              style={{ color: 'green' }}
              fontSize="default"
            />
          </IconButton>
        </Grid>
        <Grid
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
          }}
          item
          xs={12}
        >
          <IconButton
            onClick={(e) =>
              handleDelete(e, columnIndex, cardIndex, commentIndex)
            }
            className={classes.iconBtn}
          >
            <HighlightOffIcon fontSize="default" style={{ color: 'red' }} />
          </IconButton>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default CommentInput;
