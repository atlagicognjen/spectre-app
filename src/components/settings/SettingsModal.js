import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Container,
  TextField,
  Grid,
  FormGroup,
  FormControlLabel,
  Button,
  IconButton,
  Checkbox,
  Paper,
} from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { editSettings } from '../../pages/board/boardAction';
import { useSelector, useDispatch } from 'react-redux';
import SettingsIcon from '@material-ui/icons/Settings';
import { useHistory, useParams } from 'react-router';
import { deleteBoard } from '../../pages/dashboard/dashboardAction';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  paper: {
    overflowY: 'auto',
    maxHeight: '90vh',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  formItems: {
    padding: '.5rem',
  },
  button: { opacity: '.5' },
}));

export default function ModalButton() {
  const classes = useStyles();
  const { id } = useParams();
  const history = useHistory();
  const [open, setOpen] = React.useState(false);
  const { board } = useSelector((state) => state.board);
  const { boards } = useSelector((state) => state.boards);
  const [settings, setSettings] = useState({
    hideCards: false,
    disableVoting: false,
    hideVoteCount: false,
    showCardsAuthor: true,
  });

  const dispatch = useDispatch();
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (board.settings) {
      setSettings({
        hideCards: board.settings.hideCards,
        disableVoting: board.settings.disableVoting,
        hideVoteCount: board.settings.hideVoteCount,
        showCardsAuthor: board.settings.showCardsAuthor,
      });
    }
  }, [board]);
  const handleChangeSettings = (e) => {
    const { name, checked } = e.target;

    setSettings({ ...settings, [name]: checked });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(editSettings(settings, id));
    handleClose();
  };

  const handleDeleteBoard = (e) => {
    dispatch(deleteBoard(id));
    history.push('/dashboard');
  };

  return (
    <div>
      <IconButton
        className={classes.button}
        variant="contained"
        type="button"
        onClick={handleOpen}
      >
        <SettingsIcon />
      </IconButton>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Paper elevation={3} className={classes.paper}>
            <form onSubmit={handleSubmit}>
              <Grid container style={{ padding: '1rem 0' }}>
                <Grid item>
                  {' '}
                  <Grid
                    justify="center"
                    alignItems="center"
                    direction="column"
                    item
                    container
                  >
                    <Grid item className={classes.formItems}>
                      <Typography color="primary" variant="h4">
                        Settings
                      </Typography>
                    </Grid>

                    <Grid
                      style={{ textAlign: 'center' }}
                      item
                      className={classes.settings}
                    >
                      <Grid
                        container
                        justiyfy="center"
                        alignItems="center"
                        direction="column"
                      >
                        <Grid item>
                          <FormGroup className={classes.root} row={false}>
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={settings.hideCards}
                                  onChange={handleChangeSettings}
                                  name="hideCards"
                                  color="primary"
                                />
                              }
                              label="Hide cards initialy"
                            />
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={settings.disableVoting}
                                  onChange={handleChangeSettings}
                                  name="disableVoting"
                                  color="primary"
                                />
                              }
                              label="Disable voting initialy"
                            />
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={settings.hideVoteCount}
                                  onChange={handleChangeSettings}
                                  name="hideVoteCount"
                                  color="primary"
                                />
                              }
                              label="Hide vote count"
                            />
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={settings.showCardsAuthor}
                                  onChange={handleChangeSettings}
                                  name="showCardsAuthor"
                                  color="primary"
                                />
                              }
                              label="Show card's author"
                            />
                          </FormGroup>
                        </Grid>
                        <Grid item>
                          <Button
                            fullWidth={true}
                            size="small"
                            color="secondary"
                            variant="outlined"
                            onClick={handleDeleteBoard}
                          >
                            Delete Board
                          </Button>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item container justify="space-between">
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    name="save"
                    type="submit"
                    size="large"
                  >
                    Save
                  </Button>
                </Grid>
                <Grid item>
                  <Button onClick={handleClose} type="button" size="large">
                    Cancel
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Paper>
        </Fade>
      </Modal>
    </div>
  );
}
