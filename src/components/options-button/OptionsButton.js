import React, { useEffect } from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useDispatch } from 'react-redux';
import { deleteTeam } from '../../pages/teams/teamsActions';
import { isEditingTeamId } from '../../pages/teams/teamsSlice';

const options = ['Edit team name', 'Delete'];

const ITEM_HEIGHT = 48;

export default function OptionsButton({ id }) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = Boolean(anchorEl);
  const dispatch = useDispatch();

  useEffect(() => {}, [dispatch]);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    console.log(id);
  };

  const handleClose = (e) => {
    setAnchorEl(null);
  };

  const editTeams = () => {
    console.log('edit');
    dispatch(isEditingTeamId(id));
    handleClose();
  };
  const deleteTeams = () => {
    console.log('delete');
    const confirmDeletion = window.confirm('Do You realy want to delete team?');

    if (confirmDeletion) {
      dispatch(deleteTeam(id));
      handleClose();
    }
  };

  return (
    <div>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: ITEM_HEIGHT * 4.5,
            width: '20ch',
          },
        }}
      >
        {options.map((option) => (
          <MenuItem
            key={option}
            onClick={option === 'Edit team name' ? editTeams : deleteTeams}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}
