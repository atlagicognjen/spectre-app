import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoading: false,
  teamMembers: [],
  error: '',
};

const teamsSlice = createSlice({
  name: 'teamMembers',
  initialState,
  reducers: {
    newTeamMembersPending: (state) => {
      state.isLoading = true;
    },
    newTeamMembersSuccess: (state, action) => {
      state.isLoading = false;
      state.newTeamMembers = action.payload;
    },
    newTeamMembersError: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
      state.newTeamMembers = [];
    },
  },
});

const { reducer, actions } = teamsSlice;

export const {
  newTeamMembersPending,
  newTeamMembersSuccess,
  newTeamMembersError,
} = actions;

export default reducer;
