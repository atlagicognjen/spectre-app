import {
  newTeamMembersPending,
  newTeamMembersSuccess,
  newTeamMembersError,
} from './teamCardSlice';
import axios from 'axios';

export const createNewTeamMembers = (teamName) => async (dispatch) => {
  dispatch(newTeamPending());

  try {
    const { data } = await axios.post('http://localhost:5000/teams', {
      teamName,
    });
    console.log(data);
    if (!data) return dispatch(newTeamError("Can't create team"));

    dispatch(newTeamSuccess({ teamName }));
  } catch (error) {
    dispatch(newTeamError(error.message));
  }
};
