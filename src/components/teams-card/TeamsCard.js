import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import OptionsButton from '../options-button/OptionsButton';
import { Container, TextField, Grid, IconButton } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { editTeam } from '../../pages/teams/teamsActions';
import { useDispatch } from 'react-redux';
import ModalButton from '../modal/Modal';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '90%',
    marginBottom: '2rem',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  btn: { marginBottom: '1rem' },
  addMember: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  avatar: {
    backgroundColor: '#3f51b5',
  },
  formItems: {
    padding: '1rem',
  },
  members: {
    border: '1px solid gray',
    padding: '5px 20px',
    borderRadius: '25px',
    marginBottom: '1rem',
    backgroundColor: '#3f51b5',
    color: 'white',
  },
  iconBtn: { padding: '0', marginLeft: '10px', color: 'white' },
  TextField: { marginTop: '10px' },
}));

export default function TeamsCard({ team, id }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const styles = { marginBottom: '1rem' };

  const { isEditing } = useSelector((state) => state.teams);
  const [teamName, setTeamName] = useState(team.teamName);
  const [inputs, setInputs] = useState([]);

  useEffect(() => {
    setInputs(team.teamMembers);
  }, [dispatch]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setTeamName(value);
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputs];

    list[index][name] = value;
    setInputs(list);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(editTeam({ id, teamName, inputs }));
  };

  const handleAddMember = () => {
    setInputs([...inputs, { member: '' }]);
  };
  const handleDeleteMember = (e, index) => {
    const deleteInputs = [...inputs];
    deleteInputs.splice(index, 1);
    dispatch(editTeam({ id, inputs: deleteInputs }));
    setInputs(deleteInputs);
  };

  return (
    <Card raised={true} className={`${classes.root} card`}>
      {isEditing && isEditing === id ? (
        <form onSubmit={handleSubmit}>
          <TextField
            fullWidth={true}
            variant="outlined"
            className={classes.TextField}
            id="standard-basic"
            label="Team Name"
            value={teamName}
            onChange={handleChange}
            name="teamName"
            required={false}
            type="text"
          />
          <CardContent>
            <Grid container justify="center">
              {team.teamMembers.map((member, i) => (
                <Grid key={i} item>
                  <Typography className={classes.members} variant="subtitle1">
                    {member.member}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </CardContent>
          <Container>
            <Button
              className={classes.btn}
              variant="contained"
              color="primary"
              fullWidth={true}
              type="submit"
              name="edit"
            >
              Save edit
            </Button>
          </Container>
        </form>
      ) : (
        <div>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                {team.teamName[0]}
              </Avatar>
            }
            action={<OptionsButton id={id} />}
            title={<Typography variant="h6">{team.teamName}</Typography>}
            subheader="SCRUM Team"
          />

          <CardContent>
            <Grid container justify="center">
              {team.teamMembers.map((member, index) => (
                <Grid key={index} item>
                  <Typography className={classes.members} variant="subtitle1">
                    {member.member}{' '}
                    {
                      <IconButton
                        className={classes.iconBtn}
                        onClick={(e) => handleDeleteMember(e, index)}
                      >
                        <HighlightOffIcon />
                      </IconButton>
                    }
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </CardContent>
          <Container>
            <ModalButton
              fullWidth={true}
              styles={styles}
              btnText="Invite members"
              variant="contained"
              color="primary"
              fullWidth={true}
              submit={handleSubmit}
            >
              <Grid
                container
                justify="center"
                alignItems="center"
                direction="column"
              >
                <Grid item className={classes.formItems}>
                  <Typography color="primary" variant="h4">
                    Invite members
                  </Typography>
                </Grid>
                {inputs.map((input, i) => {
                  return (
                    <Grid
                      key={i}
                      item
                      container
                      direction="row"
                      alignItems="center"
                      className={classes.formItems}
                    >
                      <Grid item>
                        <TextField
                          fullWidth={true}
                          id={`${i}`}
                          value={input.member}
                          label={`Member email ${i + 1}`}
                          type="email"
                          name="member"
                          onChange={(e) => handleInputChange(e, i)}
                          required={true}
                          variant="outlined"
                        />
                      </Grid>
                    </Grid>
                  );
                })}
                <Grid item className={classes.addMember}>
                  <Button
                    color="primary"
                    onClick={handleAddMember}
                    className={classes.formItems}
                  >
                    Add member
                  </Button>
                </Grid>
              </Grid>
            </ModalButton>
          </Container>
        </div>
      )}
    </Card>
  );
}
