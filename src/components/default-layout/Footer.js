import React from 'react';

const Footer = () => {
  return (
    <div className="text center copy-right">
      &copy; Spectre - internship 2021
    </div>
  );
};

export default Footer;
