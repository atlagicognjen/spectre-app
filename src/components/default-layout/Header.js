import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import './defaultLayout.style.css';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AssignmentIcon from '@material-ui/icons/Assignment';
import GroupIcon from '@material-ui/icons/Group';
import AssessmentIcon from '@material-ui/icons/Assessment';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import { logout } from '../../components/login/loginAction';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { registerReset } from '../../components/register/registerSlice';
import { loginReset } from '../login/loginSlice';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(1),
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  btn: {
    color: 'white',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },

  icon: {
    color: 'white',
    [theme.breakpoints.up('xs')]: {
      display: 'none',
    },
    [theme.breakpoints.down('xs')]: {
      display: 'inline-block',
    },
  },
}));

export default function Header() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const handleLogout = () => {
    dispatch(logout({ history }));
    dispatch(registerReset());
    dispatch(loginReset());
  };
  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar className={classes.toolbar}>
          <Link to="/dashboard">
            <Typography className={classes.btn} variant="h5" noWrap>
              Spectre
            </Typography>
          </Link>

          <div>
            <Link to="/dashboard">
              <IconButton className={classes.icon}>
                <AssignmentIcon />
              </IconButton>
              <Button className={classes.btn}>Dashboard </Button>
            </Link>

            <Link to="/teams">
              <IconButton className={classes.icon}>
                <GroupIcon />
              </IconButton>
              <Button className={classes.btn}>Teams</Button>
            </Link>

            <Link to="/statistics">
              <IconButton className={classes.icon}>
                <AssessmentIcon />
              </IconButton>
              <Button className={classes.btn}>Statistics </Button>
            </Link>
          </div>

          <div>
            <IconButton className={classes.icon}>
              <PowerSettingsNewIcon />
            </IconButton>
            <Button onClick={handleLogout} className={classes.btn}>
              Logout
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
