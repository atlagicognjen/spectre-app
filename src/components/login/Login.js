import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button, Grid } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import { login } from './loginAction';
import { useSelector, useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  formItems: {
    margin: theme.spacing(1),
  },
}));

export default function Login() {
  const classes = useStyles();

  const [email, SetEmail] = useState('ognjen@gmail.com');
  const [password, SetPassword] = useState('asdasd');

  const dispatch = useDispatch();
  const history = useHistory();
  const { isLoading, error } = useSelector((state) => state.login);

  useEffect(() => {
    sessionStorage.getItem('user') && history.push('/dashboard');
  }, [history]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === 'email') {
      SetEmail(value);
    } else if (name === 'password') {
      SetPassword(value);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(login({ email, password, history }));
  };
  return (
    <form className={classes.root} autoComplete="off" onSubmit={handleSubmit}>
      <Grid direction="column" container>
        <Grid item className={classes.formItems}>
          <TextField
            fullWidth={true}
            id="standard-basic"
            label="Email"
            value={email}
            onChange={handleChange}
            name="email"
            required={true}
            type="email"
          />
        </Grid>
        <Grid item className={classes.formItems}>
          <TextField
            fullWidth={true}
            id="standard-password-input"
            label="Password"
            type="password"
            autoComplete="current-password"
            value={password}
            name="password"
            onChange={handleChange}
            required={true}
          />
        </Grid>
        <Grid item className={classes.formItems}>
          <Button
            fullWidth={true}
            variant="contained"
            color="primary"
            type="submit"
          >
            Login
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
