import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoading: false,
  isAuth: false,
  error: '',
};

const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    loginPending: (state) => {
      state.isLoading = true;
    },
    loginSuccess: (state) => {
      state.isLoading = false;
      state.isAuth = true;
    },
    loginError: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    loginReset: (state, action) => {
      state.isLoading = false;
      state.error = '';
      state.isAuth = false;
    },
  },
});

const { reducer, actions } = loginSlice;

export const { loginPending, loginSuccess, loginError, loginReset } = actions;

export default reducer;
