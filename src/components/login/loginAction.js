import { loginPending, loginSuccess, loginError } from './loginSlice';
import axios from 'axios';

export const login = ({ email, password, history }) => async (dispatch) => {
  dispatch(loginPending());

  try {
    const { data } = await axios.post('http://localhost:5000/users', {
      email,
      password,
    });

    if (!data) return dispatch(loginError(data.message));

    sessionStorage.setItem(
      'user',
      JSON.stringify({ email: data.email, id: data.id })
    );
    dispatch(loginSuccess());
    history.push('/dashboard');
  } catch (error) {
    dispatch(loginError(error.message));
  }
};

export const logout = ({ history }) => async (dispatch) => {
  sessionStorage.removeItem('user');
  history.push('/login');
};
