import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(() => ({
  root: {
    width: '90%',
    marginBottom: '2rem',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  btn: { marginBottom: '1rem' },
  addMember: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  avatar: {
    backgroundColor: '#3f51b5',
  },
  formItems: {
    padding: '1rem',
  },
  members: {
    border: '1px solid gray',
    padding: '5px 20px',
    borderRadius: '25px',
    marginBottom: '1rem',
    backgroundColor: '#3f51b5',
    color: 'white',
  },
  column: {
    width: '30px',
    height: '80px',
    backgroundColor: '#3f51b5',
    border: '2px solid #3f51b5',
    margin: '.2rem',
  },
  iconBtn: { padding: '0', marginLeft: '10px', color: 'white' },
  TextField: { marginTop: '10px' },
}));

export default function BoardCard({ board }) {
  const classes = useStyles();

  return (
    <Card raised={true} className={`${classes.root} card`}>
      <Link to={`/board/${board.id}`}>
        <CardHeader
          title={<Typography variant="h6">{board.boardName}</Typography>}
          subheader={`Created: ${board.createdAt}  (${board.templates.length} cards)`}
        />
        <CardContent>
          <Grid container direction="row" justify="center">
            {board.templates.map((i, idx) => (
              <Grid key={idx} item>
                <div className={classes.column}></div>
              </Grid>
            ))}
          </Grid>
        </CardContent>
      </Link>
    </Card>
  );
}
