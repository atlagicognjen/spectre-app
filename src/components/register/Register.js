import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button, Grid } from '@material-ui/core';
import { register } from './registerAction';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  formItems: {
    margin: theme.spacing(1),
  },
}));

export default function Register() {
  const classes = useStyles();

  const [name, SetName] = useState('');
  const [email, SetEmail] = useState('');
  const [password, SetPassword] = useState('');

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    sessionStorage.getItem('user') && history.push('/dashboard');
  }, [history]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === 'email') {
      SetEmail(value);
    } else if (name === 'password') {
      SetPassword(value);
    } else if (name === 'name') {
      SetName(value);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(register({ email, password, history }));
  };

  return (
    <form className={classes.root} autoComplete="off" onSubmit={handleSubmit}>
      <Grid direction="column" container>
        <Grid item className={classes.formItems}>
          <TextField
            fullWidth={true}
            id="name"
            label="Name"
            name="name"
            value={name}
            onChange={handleChange}
            required={true}
          />
        </Grid>
        <Grid item className={classes.formItems}>
          <TextField
            fullWidth={true}
            id="email"
            label="Email"
            name="email"
            value={email}
            onChange={handleChange}
            required={true}
          />
        </Grid>
        <Grid item className={classes.formItems}>
          <TextField
            fullWidth={true}
            id="password"
            label="Password"
            type="password"
            autoComplete="current-password"
            name="password"
            value={password}
            onChange={handleChange}
            required={true}
          />
        </Grid>
        <Grid item className={classes.formItems}>
          <Button
            fullWidth={true}
            variant="contained"
            color="primary"
            type="submit"
          >
            Register
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
