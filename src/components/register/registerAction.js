import {
  registerPending,
  registerSuccess,
  registerError,
} from './registerSlice';
import axios from 'axios';

export const register = ({ name, email, password, history }) => async (
  dispatch
) => {
  dispatch(registerPending());

  try {
    const { data } = await axios.post('http://localhost:5000/users', {
      name,
      email,
      password,
    });

    if (!data) return dispatch(registerError(data.message));

    sessionStorage.setItem(
      'user',
      JSON.stringify({ email: data.email, id: data.id })
    );
    dispatch(registerSuccess({ name, email, password }));
    history.push('/dashboard');
  } catch (error) {
    dispatch(registerError(error.message));
  }
};
