import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoading: false,
  user: {},
  error: '',
};

const registerSlice = createSlice({
  name: 'register',
  initialState,
  reducers: {
    registerPending: (state) => {
      state.isLoading = true;
    },
    registerSuccess: (state, action) => {
      state.isLoading = false;
      state.user = action.payload;
    },
    registerError: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
      state.user = {};
    },
    registerReset: (state, action) => {
      state.isLoading = false;
      state.error = '';
      state.user = {};
    },
  },
});

const { reducer, actions } = registerSlice;

export const {
  registerPending,
  registerSuccess,
  registerError,
  registerReset,
} = actions;

export default reducer;
